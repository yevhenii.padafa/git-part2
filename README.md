## Repo name - git-part2
## Purpose - ?? practice git

## TASK 1: Commit and push
1. Create a new branch.
2. Create file that will contain ignored file extensions and commit this
changes.
3. Add tag ‘v1.0’.
4. Check commit and tag. Push them to remote.
5. Check in gitlab UI that your changes are present.
6. Merge in master without branch deletion.




## TASK 2: Conflicts resolving
1. Create two branches from master. Name them ‘Conflict-1’ and ‘Conflict-2’.
2. To Conflict-1 branch add file with some text.
3. To Conflict-2 branch add the same file with changed word or line, for
example.
4. Merge Conflict-1 branch in master.
5. Commit changes to Conflict-2 branch and push them.
6. Merge Conflict-2 branch in master.
7. You will receive error message because of conflicts. Try to resolve them.

![Screenshot_20200725_091150.png](/images/Screenshot_20200725_091150.png)

![Screenshot_20200725_091239.png](/images/Screenshot_20200725_091239.png)

![Screenshot_20200725_091133.png](/images/Screenshot_20200725_091133.png)

![Screenshot_20200725_091950.png](/images/Screenshot_20200725_091950.png)

![Screenshot_20200725_092405.png](/images/Screenshot_20200725_092405.png)

## TASK 3: Rebasing without conflicts

1. Create branch ‘first-merged’.
2. Make some changes to existing files and commit.
3. Merge in master.
4. Create a new branch ‘second-merged’ locally without pulling changes from
master.
5. Create a new file, paste some text and commit it.
6. Checkout to master and pull changes.
7. Checkout to ‘second-merged’ and get the last changes from master using
rebase.
8. Check that now your branch has those changes, that were earlier only in
master.
9. Push it and then merge in master.



![Screenshot_20200725_092948.png](/images/Screenshot_20200725_092948.png)


![Screenshot_20200725_092932.png](/images/Screenshot_20200725_092932.png)


![Screenshot_20200725_093332.png](/images/Screenshot_20200725_093332.png)



## TASK 4: Stash your changes
1. Create a new branch.
2. Make some changes in existing files, commit and push.
3. Change the same file one more time without committing it.
4. Checkout to master. You will receive error. Try to figure out why it
happened and it what cases you will not face this error.
5. So you can stash it before switching to master. This will save locally your
changes.
6. After checkout to master, you can return to your work branch.
7. View what currently is in stash. Choose your changes.
8. Now you can continue your work.


![Screenshot_20200725_093646.png](/images/Screenshot_20200725_093646.png)

![Screenshot_20200725_093658.png](/images/Screenshot_20200725_093658.png)

![Screenshot_20200725_094058.png](/images/Screenshot_20200725_094058.png)

![Screenshot_20200725_094245.png](/images/Screenshot_20200725_094245.png)


## TASK 5: Fixing typos
1. Create branch ’typos’.
2. Create a new file ‘typos’ and write something with mistakes. Сommit it.
3. Try to change commit message without reseting it.
4. Try to reset what you previously committed without removing changes. Fix
your typo and commit one more time. View what have changed via git log.
5. Try to reset all work that you did in your new commit with complete
destruction of changes.



![Screenshot_20200725_094913.png](/images/Screenshot_20200725_094913.png)

![Screenshot_20200725_095133.png](/images/Screenshot_20200725_095133.png)

![Screenshot_20200725_095252.png](/images/Screenshot_20200725_095252.png)

![Screenshot_20200725_095518.png](/images/Screenshot_20200725_095518.png)




## Task 6: README.md formatting
This task does not require from you to be familiar with markdown, but it is nice
to have when creating your own repositories for projects.
1. Edit README with the next information:
1. The name of repository.
2. The purpose of this repository.
3. Table of contents, that will contain each task with requirements, steps
and links to merge request. Attach necessary screenshots.
4. After every task you can optionally write about your troubles that you
faced during doing it. This will help me to explain the trouble later.




